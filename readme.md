# Oauth 2
## Components

***
### Authorization Server
Keycloak

***
### Auth Server init scripts
Golang scripts creating initial configuration in the Keycloak server 
 
***
### Resource Server
A Spring boot application with configured Spring security. Redirects unauthorized users to the Authorization server. Uses session for stateful authentication.

***
### Frontend
currently not used, some simple hello world

## Auth flow
Current flow:

https://auth0.com/docs/flows/authorization-code-flow

Target solution:
in Authorizatoin code flow token is stored in the session, and the session id is stored in the cookie. This is very handy, but it is a stateful solution.

Storing the full token in the cookie and sending it to the client in the response would allow the solution to be fully stateless.  