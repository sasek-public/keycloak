package main

import (
	"sasek/keycloak-bootstrap/client"
	"sasek/keycloak-bootstrap/config"
)

func main() {
	cfg := config.LoadConfig()

	keycloakClient := client.NewKeycloakClient(cfg.KeycloakBaseUrl, cfg.RealmName)
	keycloakClient.Authenticate("admin", "admin")

	keycloakClient.CreateRealm(cfg.RealmName)
	keycloakClient.CreateClient(cfg.ClientName, cfg.RedirectUrls)
	keycloakClient.CreateUserWithRole(cfg.User.Name, cfg.User.Password, cfg.User.Role)

	keycloakClient.AddFbIntegration(cfg.Clients.Fb.IdEnvVarName, cfg.Clients.Fb.SecretEnvVarName)
	keycloakClient.AddGoogleIntegration(cfg.Clients.Google.IdEnvVarName, cfg.Clients.Google.SecretEnvVarName)
}
