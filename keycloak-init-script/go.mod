module sasek/keycloak-bootstrap

go 1.15

require github.com/buger/jsonparser v1.0.0
require gopkg.in/yaml.v2 v2.4.0
