package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
)

const FileName = "config/config.yml"

type Config struct {
	ClientName      string   `yaml:"clientName"`
	RealmName       string   `yaml:"realmName"`
	KeycloakBaseUrl string   `yaml:"keycloakBaseUrl"`
	RedirectUrls    []string `yaml:"redirectUrls"`
	User            struct {
		Role     string `yaml:"role"`
		Name     string `yaml:"name"`
		Password string `yaml:"password"`
	} `yaml:"user"`
	Clients struct {
		Fb     OAuthClient `yaml:"fb"`
		Google OAuthClient `yaml:"google"`
	} `yaml:"clients"`
}

type OAuthClient struct {
	IdEnvVarName     string `yaml:"idEnvVarName"`
	SecretEnvVarName string `yaml:"secretEnvVarName"`
}

func LoadConfig() Config {
	filename, _ := filepath.Abs(FileName)
	yamlFile, err := ioutil.ReadFile(filename)
	var config Config
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}
	return config
}
