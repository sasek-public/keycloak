package client

import (
	"encoding/json"
	"fmt"
	"github.com/buger/jsonparser"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

type KeycloakClient struct {
	baseUrl string
	client  http.Client
	Token   string
	realm   string
}

func (keycloakClient *KeycloakClient) getMasterRealmTokenUrl() string {
	return keycloakClient.baseUrl + "/auth/realms/master/protocol/openid-connect/token"
}

func (keycloakClient *KeycloakClient) getRealmUrl() string {
	return keycloakClient.baseUrl + "/auth/admin/realms"
}

func (keycloakClient *KeycloakClient) getClientUrl() string {
	return fmt.Sprintf(keycloakClient.baseUrl+"/auth/admin/realms/%s/clients", keycloakClient.realm)
}

func (keycloakClient *KeycloakClient) getRolesUrl() string {
	return fmt.Sprintf(keycloakClient.baseUrl+"/auth/admin/realms/%s/roles", keycloakClient.realm)
}

func (keycloakClient *KeycloakClient) getUsersUrl() string {
	return fmt.Sprintf(keycloakClient.baseUrl+"/auth/admin/realms/%s/users", keycloakClient.realm)
}

func (keycloakClient *KeycloakClient) getIdentityProvidersUrl() string {
	return fmt.Sprintf(keycloakClient.baseUrl+"/auth/admin/realms/%s/identity-provider/instances", keycloakClient.realm)
}

func (keycloakClient *KeycloakClient) Authenticate(user string, pass string) {
	body := strings.NewReader(fmt.Sprintf("client_id=admin-cli&username=%s&password=%s&grant_type=password", user, pass))
	res, err := http.Post(keycloakClient.getMasterRealmTokenUrl(), "application/x-www-form-urlencoded", body)
	data := readResponse(res, err)
	token, _, _, _ := jsonparser.Get(data, "access_token")
	log.Printf("Got token: %q", token)
	keycloakClient.Token = string(token)
}

func NewKeycloakClient(baseUrl string, realm string) *KeycloakClient {
	keycloakClient := new(KeycloakClient)
	keycloakClient.client = http.Client{}
	keycloakClient.realm = realm
	keycloakClient.baseUrl = baseUrl
	return keycloakClient
}

func (keycloakClient *KeycloakClient) CreateClient(clientName string, redirectUris []string) {
	createClientRequest := createClientRequest{Name: clientName, ClientId: clientName,
		RedirectUris:              redirectUris,
		StandardFlowEnabled:       true,
		DirectAccessGrantsEnabled: true,
		PublicClient:              true,
	}
	keycloakClient.postObject(createClientRequest, keycloakClient.getClientUrl())
}

type createClientRequest struct {
	Name                      string   `json:"name"`
	ClientId                  string   `json:"clientId"`
	RedirectUris              []string `json:"redirectUris"`
	StandardFlowEnabled       bool     `json:"standardFlowEnabled"`
	DirectAccessGrantsEnabled bool     `json:"directAccessGrantsEnabled"`
	PublicClient              bool     `json:"publicClient"`
}

func (keycloakClient *KeycloakClient) CreateRealm(name string) {
	bodyObj := createRealmRequest{
		Id:                   name,
		Realm:                name,
		Enabled:              true,
		RegistrationAllowed:  true,
		ResetPasswordAllowed: true,
	}
	keycloakClient.postObject(bodyObj, keycloakClient.getRealmUrl())
	keycloakClient.realm = name
}

func (keycloakClient *KeycloakClient) CreateUserWithRole(name string, password string, role string) {
	keycloakClient.createRole(role)
	keycloakClient.createUser(name)
	keycloakClient.assignUserToRole(name, role)
	keycloakClient.setUsersPassword(name, password)
}

type createRealmRequest struct {
	Id                          string `json:"id"`
	Realm                       string `json:"realm"`
	Enabled                     bool   `json:"enabled"`
	RegistrationEmailAsUsername bool   `json:"registrationEmailAsUsername"`
	RegistrationAllowed         bool   `json:"registrationAllowed"`
	ResetPasswordAllowed        bool   `json:"resetPasswordAllowed"`
}

func (keycloakClient *KeycloakClient) createRole(roleName string) {
	createRoleRequest := CreateRoleRequest{Name: roleName}
	keycloakClient.postObject(createRoleRequest, keycloakClient.getRolesUrl())
}

type CreateRoleRequest struct {
	Name string `json:"name"`
}

func (keycloakClient *KeycloakClient) createUser(username string) {
	createUserRequest := createUserRequest{Username: username, Enabled: true}
	jsonBody, _ := json.Marshal(createUserRequest)
	req, _ := http.NewRequest("POST", keycloakClient.getUsersUrl(), strings.NewReader(string(jsonBody)))
	keycloakClient.executeRequest(req, 201)
}

type createUserRequest struct {
	Username string `json:"username"`
	Enabled  bool   `json:"enabled"`
}

func (keycloakClient *KeycloakClient) assignUserToRole(userName string, roleName string) {
	userId := keycloakClient.findUserIdByName(userName)
	roleId := keycloakClient.findRoleByName(roleName)
	roleMappingsUrl := fmt.Sprintf("%s/%s/role-mappings/realm", keycloakClient.getUsersUrl(), userId)
	request := assignRoleRequest{
		ClientRole:  false,
		Id:          roleId,
		Name:        roleName,
		Composite:   false,
		ContainerId: "master",
	}
	jsonBytea, _ := json.Marshal(request)
	body := "[" + string(jsonBytea) + "]"
	req, _ := http.NewRequest("POST", roleMappingsUrl, strings.NewReader(body))
	keycloakClient.executeRequest(req, 204)
}

type assignRoleRequest struct {
	ClientRole  bool   `json:"clientRole"`
	Id          string `json:"id"`
	Name        string `json:"name"`
	ContainerId string `json:"containerId"`
	Composite   bool   `json:"composite"`
}

func (keycloakClient *KeycloakClient) setUsersPassword(userName string, password string) {
	userId := keycloakClient.findUserIdByName(userName)
	usersUrl := fmt.Sprintf("%s/%s/reset-password", keycloakClient.getUsersUrl(), userId)
	bodyObj := setCredentialsRequest{
		Temporary: false,
		Type:      "password",
		Value:     password,
	}
	bodyJson, _ := json.Marshal(bodyObj)
	req, _ := http.NewRequest("PUT", usersUrl, strings.NewReader(string(bodyJson)))
	keycloakClient.executeRequest(req, 204)
}

type setCredentialsRequest struct {
	Temporary bool   `json:"temporary"`
	Type      string `json:"type"`
	Value     string `json:"value"`
}

// internal

func (keycloakClient *KeycloakClient) findUserIdByName(name string) string {
	usersUrl := fmt.Sprintf("%s?username=%s", keycloakClient.getUsersUrl(), name)
	req, _ := http.NewRequest("GET", usersUrl, nil)
	response := keycloakClient.executeRequest(req, 200)
	userId, _, _, _ := jsonparser.Get(response, "[0]", "id")
	log.Printf("Found user: %s with id: %s", name, userId)
	return string(userId)
}

func (keycloakClient *KeycloakClient) findRoleByName(name string) string {
	rolesUrl := fmt.Sprintf("%s/%s", keycloakClient.getRolesUrl(), name)
	req, _ := http.NewRequest("GET", rolesUrl, nil)
	response := keycloakClient.executeRequest(req, 200)
	userId, _, _, _ := jsonparser.Get(response, "id")
	log.Printf("Found user: %s with id: %s", name, userId)
	return string(userId)
}

//identity providers

func (keycloakClient *KeycloakClient) AddFbIntegration(fbIdEnvVar string, fbSecretEnvVar string) {
	fbClientId := os.Getenv(fbIdEnvVar)
	fbClientSecret := os.Getenv(fbSecretEnvVar)
	log.Printf("Facebook credentials: client %s. secret %s", fbClientId, mask(fbClientSecret))
	keycloakClient.createIdentityProvider("facebook", fbClientId, fbClientSecret)
}

func (keycloakClient *KeycloakClient) AddGoogleIntegration(googleIdEnvVar string, googleSecretEnvVar string) {
	googleClientId := os.Getenv(googleIdEnvVar)
	googleClientSecret := os.Getenv(googleSecretEnvVar)
	log.Printf("Google credentials: client %s. secret %s", googleClientId, mask(googleClientSecret))
	keycloakClient.createIdentityProvider("google", googleClientId, googleClientSecret)
}

func (keycloakClient *KeycloakClient) createIdentityProvider(providerId string, clientId string, clientSecret string) {
	bodyObj := createIdentityProviderRequest{
		Config: IdentityProviderConfig{
			ClientId:     clientId,
			ClientSecret: clientSecret,
			UseJwksUrl:   "true",
			SyncMode:     "IMPORT",
		},
		Alias:                     providerId,
		ProviderId:                providerId,
		Enabled:                   true,
		AuthenticateByDefault:     false,
		FirstBrokerLoginFlowAlias: "first broker login",
	}
	keycloakClient.postObject(bodyObj, keycloakClient.getIdentityProvidersUrl())
}

type createIdentityProviderRequest struct {
	Config                    IdentityProviderConfig `json:"config"`
	Alias                     string                 `json:"alias"`
	ProviderId                string                 `json:"providerId"`
	Enabled                   bool                   `json:"enabled"`
	AuthenticateByDefault     bool                   `json:"authenticateByDefault"`
	FirstBrokerLoginFlowAlias string                 `json:"firstBrokerLoginFlowAlias"`
}

type IdentityProviderConfig struct {
	ClientId     string `json:"clientId"`
	ClientSecret string `json:"clientSecret"`
	UseJwksUrl   string `json:"useJwksUrl"`
	SyncMode     string `json:"syncMode"`
}

// technical

func readResponse(res *http.Response, err error) []byte {
	if err != nil {
		log.Fatal(err)
	}
	data, _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()
	return data
}

func (keycloakClient *KeycloakClient) postObject(jsonObj interface{}, url string) []byte {
	jsonBytea, _ := json.Marshal(jsonObj)
	body := strings.NewReader(string(jsonBytea))
	req, _ := http.NewRequest("POST", url, body)
	return keycloakClient.executeRequest(req, 201)
}

func (keycloakClient *KeycloakClient) executeRequest(req *http.Request, expectedStatus int) []byte {
	req.Header.Add("Authorization", "Bearer "+keycloakClient.Token)
	req.Header.Add("Content-Type", "application/json")
	log.Printf("Req Method: %s, Req url: %s", req.Method, req.URL)
	res, _ := keycloakClient.client.Do(req)
	log.Printf("response status: %d", res.StatusCode)
	responseBody, err := ioutil.ReadAll(res.Body)

	if err != nil {
		log.Fatal("Response error")
	}

	if !(res.StatusCode == expectedStatus) {
		log.Fatal(fmt.Sprintf("Invalid response status: %d, expected: %d", res.StatusCode, expectedStatus))
	}

	return responseBody
}

func mask(s string) string {
	rs := []rune(s)
	const maskedCharacters = 6
	for i := 0; i < len(rs)-maskedCharacters; i++ {
		rs[i] = 'X'
	}
	return string(rs)
}
