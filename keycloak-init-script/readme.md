## Prerequisites
Go lang installed

https://golang.org/doc/install

## How to run:
1. Check config values
2. For Facebook and Google integrations, create apps and set their clientIds and client secrets as env variables referred in config.

        Facebook login documentation:   
        https://developers.facebook.com/docs/facebook-login/web
        
        Google login documentation:
        https://developers.google.com/adwords/api/docs/guides/authentication
   
3. When Keycloak is running, `go run keycloak_init.go`


## Created realm console:

http://localhost:8090/auth/admin/keycloak-poc/console/

## Todo
### Clustered setup:
https://github.com/ivangfr/keycloak-clustered
