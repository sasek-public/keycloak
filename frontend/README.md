# Frontend app (not integrated)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

***

### Stateless authentication management
https://stackoverflow.com/questions/36751304/jwt-token-strategy-for-frontend-and-backend
