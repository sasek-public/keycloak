package cloud.sasek.helloworld;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class HelloWorldController {

  private final HelloService helloService;

  @GetMapping("/public/hello")
  public String sayHello() {
    return "Hello, world!";
  }

  @GetMapping("/hello/{name}")
  public String sayHello(@PathVariable("name") String name) {
    return helloService.sayHello(name);
  }

  @GetMapping("/hellome")
  public String helloMe(Authentication authentication) {
    return String.format("Your username is %s", authentication.getName());
  }

}
