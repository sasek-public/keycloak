package cloud.sasek.helloworld

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import spock.lang.Specification

class Oauth2IntegrationLearningTest extends Specification {
    def "Succussfull request to the public endpoint"() {
        given:
        def restTemplate = new RestTemplateBuilder().build()

        when:
        def responseEntity = restTemplate.getForEntity("http://localhost:8091/public/hello", String.class)

        then:
        responseEntity.statusCode == HttpStatus.OK
        responseEntity.body == "Hello, world!"
    }

    def "when trying to access protected endpoint without authentication gets the login page"() {
        given:
        def restTemplate = new RestTemplateBuilder().build()

        when:
        def responseEntity = restTemplate.getForEntity(new URI("http://localhost:8091/hello/jim"), String.class)

        then:
        responseEntity.body.contains("Please sign in")
    }

    def "Successfully authenticated with OAuth2 token"() {
        given:
        def restTemplate = new RestTemplateBuilder().build()
        def token = authenticate()

        def requestEntity = RequestEntity.get(new URI("http://localhost:8091/hello/jim"))
                .header("AUTHORIZATION", "Bearer " + token)
                .build()

        when:
        def responseEntity = restTemplate.exchange(requestEntity, String.class)

        then:
        responseEntity.statusCode == HttpStatus.OK
        responseEntity.body == "Hello, jim!"
    }

    String authenticate() {
        HttpHeaders headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("client_id", "springBootClient");
        map.add("username", "username");
        map.add("password", "1234");
        map.add("grant_type", "password");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers)

        def restTemplate = new RestTemplateBuilder().build()
        def response = restTemplate.postForEntity(new URI("http://localhost:8090/auth/realms/master/protocol/openid-connect/token"), request, AuthResponse.class)
        return response.body.accessToken
    }

    static class AuthResponse {
        @JsonProperty("access_token")
        public String accessToken;
    }
}
