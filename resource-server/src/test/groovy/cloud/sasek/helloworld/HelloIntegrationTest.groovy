package cloud.sasek.helloworld

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@SpringBootTest
@AutoConfigureMockMvc
class HelloIntegrationTest extends Specification {
    @Autowired
    private MockMvc mockMvc

    def "hello, world"() throws Exception {
        given:
        // context loaded

        when:
        mockMvc.perform(get("/public/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello, world!"));

        then:
        noExceptionThrown()
    }

    @WithMockUser
    def "hello, jony"() throws Exception {
        given:
        // context loaded

        when:
        mockMvc.perform(get("/hello/jony"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello, jony!"));

        then:
        noExceptionThrown()
    }
}
