### Resource server
Test:

`~>./gradlew clean test `

Build:

`~>./gradlew clean build`

Run directly:

`./gradlew bootRun`

#### Docker

Build docker image:

`~>./gradlew dockerBuildImage`

Run docker image:

`~>docker run --name sasek-hello -ti --rm -p 8080:8080 hello-world-java`

***
based on:

https://github.com/hantsy/keycloak-springsecurity5-sample
